import java.awt.Point;
import java.util.LinkedList;


public class Reaper {
	
	private static int instances = 0;
	private static LinkedList<Point> reaperPos= new LinkedList<Point>();
	private int instanceRef;
	
	private int xPos, yPos;
	
	public Reaper(){
		
		xPos = 0;	yPos = 0;
		reaperPos.add(new Point(xPos, yPos));
		
		instanceRef = instances;
		instances++;
	}

	public int getxPos() {return xPos;}

	public int getyPos() {return yPos;}
	
	public void setPosition(int x, int y){
		reaperPos.remove(instanceRef);
		this.xPos = x;	this.yPos = y;
		reaperPos.add(instanceRef, new Point(xPos, yPos));
	}
	
	public void test(){
		
		System.out.println(reaperPos.get(instanceRef).x + ", " + reaperPos.get(instanceRef).y);
		
	}
	
	public void trackplayer(int xP, int yP){
		
		System.out.println(reaperPos.get(instanceRef).x + ", " + reaperPos.get(instanceRef).y);
		
		reaperPos.remove(instanceRef);
		
		//Delta x is larger
		if(Math.abs(xP - xPos) >=Math.abs(yP - yPos)){
			
			
			if(xP > xPos){
				
				if(isValid(xPos + 1, yPos)){
					xPos++;		
					reaperPos.add(instanceRef, new Point(xPos, yPos));
					return;
				}
			}
			
			else{
				
				if(isValid(xPos - 1, yPos)){
					xPos--;		
					reaperPos.add(instanceRef, new Point(xPos, yPos));
					return;
				}
			}
			
			
		}
		
		//Delta y is larger or no valid x move	
		else{
			

			if(yP > yPos){
				
				if(isValid(xPos, yPos + 1)){
					yPos++;	
					reaperPos.add(instanceRef, new Point(xPos, yPos));
					return;
				}
			}
			
			else{
				
				if(isValid(xPos, yPos - 1)){
					yPos--;
					reaperPos.add(instanceRef, new Point(xPos, yPos));
					return;
				}
			} 
		 }
		
		reaperPos.add(instanceRef, new Point(xPos, yPos));
		
	}
	
	public boolean isValid(int x, int y){
		
		if(x > 6 || x < 0 || y < 0 || y > 6)
			return false;
		
		for(int i =0; i< reaperPos.size(); i++){
			
			if(reaperPos.get(i).x == x && reaperPos.get(i).y == y)
				return false;
			
		}
		
		return true;
		
	}

}
