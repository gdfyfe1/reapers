import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;

public class apples extends JFrame implements Runnable, MouseListener{

	public static final String title= "Reapers";
	public static final int width = 800;
	public static final int height = 800;
	
	public static Thread mainLoop;
	public static boolean running = false;
	
	public static final int gridX = 7;
	public static final int gridY = 7;
	
	public static Player player;
	public static Boolean playerMove = true;
	
	public static Reaper[] reaper= new Reaper[4];
	
	public static void main(String[] args){
		
		new apples();
	}
	
	public apples(){
		
		super(title);
		setSize(width, height);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		player = new Player();
		
		for(int i = 0; i< 4; i++){
			
			reaper[i] = new Reaper();
		}
		
		reaper[0].setPosition(0,  0);
		reaper[1].setPosition(6,  0);
		reaper[2].setPosition(0,  6);
		reaper[3].setPosition(6,  6);
		
		addMouseListener(this);
	}
	
	public void paint(Graphics g){
		
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		
		g.setColor(Color.black);
		
		for(int x =0; x< gridX; x++){
			for(int y =0; y < gridY; y++){
				
				g.drawRect(x * 90 + 50, y * 90 + 50, 90, 90);
				
			}
		}
		
		if (playerMove){
			
			g.setColor(new Color(255, 200, 0, 40));
			
			for(int x = -2; x < 3; x++){
				
				for(int y = (-2 + Math.abs(x)); y < (5 - (Math.abs(x) * 2)) + (-2 + Math.abs(x)); y++){
					
					if(player.getxPos() + x > 6 || player.getxPos() + x < 0 || player.getyPos() + y > 6 || player.getyPos() + y < 0){}
				
					else
						g.fillRect((player.getxPos() + x) * 90 + 50, (player.getyPos() + y) * 90 + 50, 90, 90);	
				
				}
			}
		}
		
		
		
		//Draw player
		g.setColor(Color.cyan);
		g.fillOval(player.getxPos() * 90 + 55, player.getyPos() * 90 + 55, 80, 80);
		
		//Draw reapers
		g.setColor(Color.black);
		
		for(int i =0; i< 4; i++){
			g.setColor(Color.black);
			g.fillOval(reaper[i].getxPos() * 90 + 55, reaper[i].getyPos() * 90 + 55, 80, 80);
			g.setColor(Color.white);
			g.drawString(i + "", reaper[i].getxPos() * 90 + 90, reaper[i].getyPos() * 90 + 100);
		}
	}
	
	public void start(){
		if(!running){
			
			running = true;
			mainLoop = new Thread(this);
			mainLoop.start();
		}
		
	}

	@Override
	public void run() {
		
		while(Thread.currentThread() == mainLoop){

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if(running){
				
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		
		if(playerMove){
			
			if(player.movePlayer((e.getX() - 50) / 90, (e.getY() - 50) / 90)){
				playerMove = false;
			}
			else{
				System.out.println("Invalid move");
			}
			
			repaint();
		}
		else{
			System.out.println("not your turn");
		}
		
		moveReapers();
	}

	public void moveReapers() {
		
		for(int i= 0; i< 4; i++){
				
			reaper[i].trackplayer(player.getxPos(), player.getyPos());
			repaint();
			try {
				Thread.sleep(750);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		playerMove = true;
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {}
}
