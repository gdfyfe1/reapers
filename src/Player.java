
public class Player {
	
	private int xPos, yPos;

	public int getxPos() {return xPos;}
	public void setxPos(int xPos) {this.xPos = xPos;}

	public int getyPos() {return yPos;}
	public void setyPos(int yPos) {this.yPos = yPos;}
	
	public void setPosition(int x, int y){
		this.xPos = x;	this.yPos = y;
	}
	
	public Player(){
		
		xPos = 3;
		yPos = 3;
	}
	
	public boolean movePlayer(int xD, int yD){
		
		
		if(xD > 6 || xD < 0 || yD < 0 || yD > 6)
			return false;
		
		
		for(int x = -2; x < 3; x++){
			for(int y = (-2 + Math.abs(x)); y < (5 - (Math.abs(x) * 2)) + (-2 + Math.abs(x)); y++){
				
					
				if(xPos + x == xD && yPos + y == yD){
					xPos = xD;	yPos = yD;
					return true;
				}
			}
		}
		
		return false;
	}

}
